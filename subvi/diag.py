# -*- coding: utf-8 -*-
import logging
import time
import datetime
import os
import numpy as np
import pandas as pd
from pandas import Series
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import kpss
from collections import OrderedDict
import matplotlib.pyplot as plt
st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')	

def session_start(path):
	logging.basicConfig(filename=log_file_name(path, "_log"),level=logging.DEBUG) #so that path is passed
	logging.info("*** Check Start at: %s ***" % (st))

# @path: path to logging file
# @core_number: current core holder to check
# @ads_des: is the system in adsorption?
def nano_cond_fun(path, core_number, adsorption, pulsetime_in):
	# Check for path first to prevent crash

	if(path):
		# Load raw data
		df = pd.read_csv(path, delimiter="\t")

		# Get checking interval from INI file and get minimum no. of points needed for check
		check_interval = int(open(ini_file_name(path), "r").readlines()[15].split(" = ")[1])
		num_of_points = check_interval - 60
		# global pressure_pulse
		pressure_pulse = float(open(ini_file_name(path), "r").readlines()[16].split(" = ")[1])

		# global d
		d = OrderedDict()
		d['date_time'] = st
		for cn in range(1,5):
			if (cn == core_number):
				# That's the actual core for evaluation
				
				# load pressure and mass data for corresponding core holder
				# global data_press
				data_press = df["Rosemount"+str(core_number)][-num_of_points:]
				data_mass  = df["Balance"+str(core_number)][-num_of_points:]

				# Check if enough data is already logged
				if (len(data_press) >= num_of_points):
					# Enough data exist, do the evaluation
					decision = is_stable(path, data_press, 1)

					if(decision):
						# Decision = 1, will move to next pressure point
						# global isotherm_press
						data = data_press
						sub = data[-120:] # average last 60 points not # sub = data[int(np.round(data.shape[0]*0.5)):data.shape[0]-20]
						isotherm_press = np.round_(np.mean(sub),3)

						data = data_mass
						sub = data[-120:] # average last 60 points
						isotherm_mass = np.round_(np.mean(sub),3)

						# Determine pulse_time
						if pulsetime_in > 0:
							pulse_time = pulsetime_in
						else:
							# no pulsetime_# provided
							# Fixed Numbers
							if(adsorption):
								pulse_time = 250
							else:
								if (isotherm_press > 0):
									pulse_time = 1500
								else:
									pulse_time = 2500
									

						# global dflogged
						# dflogged = pd.read_csv(log_file_name(path, "_logging"), delimiter=",")
						# dflogged = dflogged.loc[:, ['date_time', 'Mass_'+str(cn), 'Pressure_'+str(cn), 'Pulse_Time_'+str(cn)]].dropna()
						# if dflogged.size > 0:
							# we have previous pulse_time to calculate new time
							# pulse_time = round(dflogged['Pulse_Time_'+str(cn)].iloc[-1] / (abs(isotherm_press - dflogged['Pressure_'+str(cn)].iloc[-1])/pressure_pulse))
							# if pulse_time > 10000:
								# pulse_time = 10000;
						# else:
							# no previous pulse_time to calculate based on
							# Fixed Numbers
							# if(adsorption):
								# pulse_time = 250
							# else:
								# pulse_time = 1250
								# if (isotherm_press > 0):
									# pulse_time = 1500
								# else:
									# pulse_time = 2500

					else:
						decision = 0
						pulse_time = 0
						isotherm_press = float('nan')
						isotherm_mass  = float('nan')
				else:
					# No enough data provided yet
					decision = 0
					pulse_time = 0
					isotherm_press = float('nan')
					isotherm_mass  = float('nan')

				# Fill the logging row
				d['Pressure_'+str(cn)] 	 = [isotherm_press]
				d['Mass_'+str(cn)] 		 = [isotherm_mass]
				d['Pulse_Time_'+str(cn)] = pulse_time
				d['Decision_'+str(cn)] 	 = decision
			else:
				# Don't evaluate, fill NaNs
				decision = 0
				pulse_time = 0
				d['Pressure_'+str(cn)]   = [float('nan')]
				d['Mass_'+str(cn)] 		 = [float('nan')]
				d['Pulse_Time_'+str(cn)] = pulse_time
				d['Decision_'+str(cn)] 	 = decision

		# End of for loop and logging row formation
		# Write the logging row to file
		dfiso = pd.DataFrame(data=d)
		with open(log_file_name(path, "_logging"), 'a') as f: #use this for f.tell
			dfiso.to_csv(f, header=f.tell()==0, index=False, mode = 'a')

		# Extract Actual Values Corresponding to current core_number #
		decision   = d['Decision_'+str(core_number)]
		pulse_time = d['Pulse_Time_'  +str(core_number)]

		# Extra Logging #
		logging.basicConfig(filename=log_file_name(path, "_log"),level=logging.DEBUG) #so that path is passed
		lastValue = df["Rosemount"+str(core_number)][-1:]
		logging.info("Core Holder: %s - Decision: %s - Pulse Time: %s - Last Value: %f" % (str(core_number), str(decision), str(pulse_time), lastValue))
		return ([decision, pulse_time])
	else:
		# No path provided (no logging in LabVIEW)
		return ([0, 0])

def session_end(path):
	logging.basicConfig(filename=log_file_name(path, "_log"),level=logging.DEBUG) #so that path is passed
	logging.info("*** Check End ***\n")

def is_stable(path, data, conf = 1):
	"""
	Uses both the ADF and KPSS tests to determine if the data is time-stationary
	INPUT:   data = pandas data Series of values
			confidence = confidence interval. Select 1 (for p=0.01) or 5 (for p=0.05)
			For ADF: The more negative it is, the stronger the rejection of the hypothesis that there is a unit root
	OUTPUT: int True (1) or False (0)
	"""
	import warnings
	warnings.simplefilter(action='ignore')
	data=data[~data.isnull()].values

	adf_results  = adfuller(data)
	kpss_results = kpss(data)
	plt.plot(data)

	logging.basicConfig(filename=log_file_name(path, "_log"),level=logging.DEBUG) #so that path is passed
	logging.info("adf_test: %s - kpss_test: %s" % (str(adf_results[0]), str(kpss_results[0])))

	# directly compare with the built-in confidence levels
	if (adf_results[0] < adf_results[4][str(conf)+'%'] and \
	   kpss_results[0] < kpss_results[3][str(conf)+'%']):
		return 1
	else:
		return 0

def log_file_name(path, extension):
	logFilename = os.path.basename(os.path.splitext(path)[0])
	logDirname = os.path.dirname(path)
	return logDirname+"\\"+logFilename+extension+".txt"

def ini_file_name(path):
	logDirname = os.path.dirname(path)
	return logDirname+"/nano-condensation.ini"